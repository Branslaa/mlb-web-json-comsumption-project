﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace MLBProject
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		Rootobject mlbData = new Rootobject();
		List<Rootobject> mlbDayList = new List<Rootobject>();
		public MainWindow()
		{
			InitializeComponent();
			fillYearDDL();

			StreamReader read = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "year.txt");
			string yearFromFile = read.ReadLine();
			read.Close();

			cboYear.SelectedValue = int.Parse(yearFromFile);
		}

		private void fillYearDDL()
		{
			for (int x = 2011; x <= DateTime.Now.Year; x++)
			{
				cboYear.Items.Add(x);
			}

		}

		private void saveYearFile()
		{
			string path = AppDomain.CurrentDomain.BaseDirectory + "year.txt";
			if (File.Exists(path))
			{
				File.Delete(path);
			}

			File.Create(path);
			File.AppendAllText(path, cboYear.SelectedValue.ToString());
		}

		private string getURL(string year, string month, string day)
		{
			return string.Format("http://gd2.mlb.com/components/game/mlb/year_{0}/month_{1}/day_{2}/master_scoreboard.json", year, addZero(month), addZero(day));
		}

		private string addZero(string num)
		{
			switch (num)
			{
				case "1":
				case "2":
				case "3":
				case "4":
				case "5":
				case "6":
				case "7":
				case "8":
				case "9":
					return "0" + num;
				default:
					return num;
			}
		}

		public static IEnumerable<DateTime> AllDaysInMonth(int year, int month)
		{
			int days = DateTime.DaysInMonth(year, month);
			for (int day = 1; day <= days; day++)
			{
				yield return new DateTime(year, month, day);
			}
		}

		private void getJson(int x)
		{
			MessageBox.Show("Loading new year, please be patient");
			cboMonth.Items.Clear();
			cboGame.Items.Clear();
			lstHome.Items.Clear();
			lstAway.Items.Clear();
			lstPitchersWinning.Items.Clear();
			lstPitchersLosing.Items.Clear();
			lstLinks.Items.Clear();

			DateTime date = new DateTime(x, 1, 1);
			string path = AppDomain.CurrentDomain.BaseDirectory + "MLB_" + x + ".json";

			if (File.Exists(path))
			{
				File.Delete(path);
				File.Create(path);
			}

			while (date.Year == x)
			{
				string url = getURL(x.ToString(), date.Month.ToString(), date.Day.ToString());
				date = date.AddDays(1);
				try
				{
					WebClient client = new WebClient();
					string json = client.DownloadString(url);
					client.Dispose();

					mlbData = JsonConvert.DeserializeObject<Rootobject>(json);
					mlbDayList.Add(mlbData);
					string newJSON = JsonConvert.SerializeObject(mlbData);

					File.AppendAllText(path, newJSON);

				}
				catch { }
			}

			MessageBox.Show("All Done!");
			populateMonthDDL();
		}

		private void populateMonthDDL()
		{
			cboMonth.Items.Clear();
			foreach (Rootobject data in mlbDayList)
			{
				if (data.data.games.game != null)
					cboMonth.Items.Add(data);
			}
		}

		private void cboYear_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			getJson(int.Parse(cboYear.SelectedItem.ToString()));
		}

		private void cboMonth_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (cboMonth.SelectedIndex >= 0)
				populateGameDDL();
		}

		private void populateGameDDL()
		{
			cboGame.Items.Clear();
			Rootobject value = (Rootobject)cboMonth.SelectedItem;
			for (int x = 0; x < value.data.games.game.Count(); x++)
			{
				cboGame.Items.Add(value.data.games.game[x]);
			}
		}
		private void cboGame_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			lstHome.Items.Clear();
			lstAway.Items.Clear();
			lstPitchersWinning.Items.Clear();
			lstPitchersLosing.Items.Clear();
			lstLinks.Items.Clear();

			lstHome.Items.Add(cboGame.SelectedItem);
			lstAway.Items.Add(cboGame.SelectedItem);
			lstPitchersWinning.Items.Add(cboGame.SelectedItem);
			lstPitchersLosing.Items.Add(cboGame.SelectedItem);

			lstLinks.Items.Add(cboGame.SelectedItem);

		}

		private void btnSaveDate_Click(object sender, RoutedEventArgs e)
		{
			saveYearFile();
		}
	}
}
